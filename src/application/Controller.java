package application;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

import java.io.*;
import java.net.URL;
import java.sql.*;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.function.IntConsumer;

public class Controller implements Initializable {

    // Скрипт создания таблицы с настройками
    private final String createSettingsTable =
            "create table settingstable (" +
            "    availableCountLines    int,\n" +
            "    readSpeedMillis        int,\n" +
            "    oddCellsColor          varchar(8),\n" +
            "    evenCellsColor         varchar(8),\n" +
            "    lang                   varchar(3)\n" +
            ");";

    // Скрипт проверки наличия таблицы с настройками в БД
    private final String checkSettingsTableExist =
            "SELECT relname FROM pg_class where relname='settingstable';";

    // Скрипт для получения настроек с базы данных
    private final String selectSettings =
            "select availableCountLines, readSpeedMillis, oddCellsColor, evenCellsColor, lang\n" +
            "from settingstable;";

    // Скрипт для ужаления всех настроек
    private final String removeSettings =
            "delete from settingstable;";

    // Скрипт для сохранения настроек
    private final String saveSettings =
            "insert into settingstable (availableCountLines, readSpeedMillis, oddCellsColor, evenCellsColor, lang) \n" +
            "values (?, ?, ?, ?, ?);";

    // CSS стиль для списка потоков
    private final String linesAreaCSS =
            ".list-cell:odd {\n" +
            "    -fx-background-color: %s;\n" +
            "}\n" +
            ".list-cell:even {\n" +
            "    -fx-background-color: %s;\n" +
            "}\n" +
            ".list-cell:selected {\n" +
            "    -fx-background-color: %s;\n" +
            "}\n";

    // Параметры подключения к базе данных
    private final String databaseUrl = "jdbc:postgresql://192.168.56.102:5432/settings";
    private final String databaseName = "the";
    private final String databasePassword = "password";

    // Элемент для отображения списка строк. ListView выбран для того,
    // чтобы легче было работать с количеством строк.
    // Так же он автоматически прокручивается вниз, если опустить полосу прокрутки до конца
    @FXML
    private ListView<String> linesAreaListView;
    private final ObservableList<String> linesList = FXCollections.observableArrayList();

    @FXML
    private ListView<LinesReader> linesReadersListView;     // Список потоков для чтения строк из файла
    private final ObservableList<LinesReader> linesReaders = FXCollections.observableArrayList();

    @FXML
    private TextField readSpeedTextField;   // Текстовое поле с периодом считывания строк
    long readSpeedMillis = 5000;            // Период считывания строк

    @FXML
    private TextField availableCountLinesTextField;     // Текстовое поле с количеством строк для отображения
    int availableCountLines = 200;                      // Количество строк для отображения


    @FXML
    private ColorPicker oddLinesColorPicker, evenLinesColorPicker;  // Цвета четных и нечетных строк списка потоков

    @FXML
    private ComboBox<String> langComboBox;      // Выбор языка

    // Списки всех элементов на сцене, которые отображают текст (для смены языка)

    @FXML
    private Button startThreadButton,
                   stopThreadButton,
                   saveSettingsButton,
                   loadSettingsButton;

    @FXML
    private Tab linesAreaTab,
                controlPanelTab,
                settingsTab;

    @FXML
    private Label availableCountLinesLabel,
                  readSpeedLabel,
                  oddLinesColorLabel,
                  evenLinesColorLabel,
                  langChooserLabel;

    ResourceBundle resources;       // Содержатся строки для перевода

    Connection connection = null;   // Подключение к базе данных

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.resources = resources;
        linesAreaListView.setItems(linesList);
        linesReadersListView.setItems(linesReaders);
        readSpeedTextField.textProperty().addListener(
                new IntegerStringChangeListener(readSpeedTextField, this::setReadSpeed));
        availableCountLinesTextField.textProperty().addListener(
                new IntegerStringChangeListener(availableCountLinesTextField, this::setAvailableCountLines));
        linesList.addListener((ListChangeListener<? super String>) c -> removeExtraString());
        oddLinesColorPicker.valueProperty().addListener(this::colorChanged);
        evenLinesColorPicker.valueProperty().addListener(this::colorChanged);

        try {
            Class.forName("org.postgresql.Driver"); // Загрузка драйвера
            connection = DriverManager.getConnection(databaseUrl, databaseName, databasePassword);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Вызывается при изменении цвета одного из ColorPicker.
     * Происходит сохранение CSS со стилем для списка потоков во временный файл,
     * после этого загрузка файла в соответственный ListView. Это самый безобидный способ из рассмотренных:
     * -   Метод setStyle(String style) не помогает, так как он не применяется рекурсивно
     * ко всем потомкам.
     * -   Можно создать собственную CellFactory, но тогда придется самому
     * отслеживать изменение цветов и учитывать выделенные элементы, получается много ненужного кода.
     * -   Можно создать свой URI протокол, но это больше похоже на костыль.
     * -   data URI схема почему-то не работает.
     */
    private void colorChanged(ObservableValue<? extends Color> observable, Color oldValue, Color newValue) {
        Color oddLinesColor = oddLinesColorPicker.getValue();
        Color evenLinesColor = evenLinesColorPicker.getValue();
        try {
            File cssFile = File.createTempFile("test", ".css");
            PrintWriter out = new PrintWriter(cssFile);
            out.printf(linesAreaCSS, toHexString(oddLinesColor), toHexString(evenLinesColor), "blue");
            out.close();
            linesReadersListView.getStylesheets().setAll(cssFile.toURI().toString());
            cssFile.delete();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод для преобразования цвета в #RrGgBb формат, Например: #89ABCD
     * @param color цвет для преобразование
     * @return Строка, равная цвету в формате #RrGgBb
     */
    private String toHexString(Color color) {
        return String.format( "#%02X%02X%02X",
                (int)(color.getRed() * 255),
                (int)(color.getGreen() * 255),
                (int)(color.getBlue() * 255));
    }

    /**
     * Установка количетсва строк для отображения в текстовом элементе
     * @param availableCountLines количетсво строк для отображения в текстовом элементе
     */
    public void setAvailableCountLines(int availableCountLines) {
        this.availableCountLines = availableCountLines;
        removeExtraString();
    }

    /**
     * Метод, удаляющий лишние строки, если их больше чем availableCountLines
     */
    private void removeExtraString() {
        Platform.runLater(() -> {
            int countExtraLines = linesList.size() - availableCountLines;
            if (countExtraLines > 0)
                linesList.remove(0, countExtraLines);
        });
    }

    /**
     * Установка периода для считывания строки из файла
     * @param readSpeedMillis скорость считывания строки из файла в миллисекундах
     */
    private void setReadSpeed(long readSpeedMillis) {
        this.readSpeedMillis = readSpeedMillis;
        linesReaders.stream().forEach(linesReader -> linesReader.setReadSpeed(readSpeedMillis));
    }

    /**
     * Метод вызывается после нажания на кнопку создания нового потока для считывания из файла
     */
    @FXML
    private void startThreadAction(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Text Files", "*.txt"));
        File selectedFile = fileChooser.showOpenDialog(null);
        if (selectedFile == null)
            return;
        LinesReader linesReader;
        try {
            linesReader = new LinesReader(selectedFile, readSpeedMillis, this::addLine, this::removeLinesReader);
        } catch (FileNotFoundException e) {
            return;
        }
        linesReaders.add(linesReader);
        linesReader.start();
    }

    /**
     * Добовление новой строки в список
     * @param string строка для добавления в список
     */
    private void addLine(String string) {
        // Так как в процессе добавления строки будет изменен элемент интерфейса,
        // то это необходимо выполнять из потока работы с интерфейсом.
        // Следовательно никакая синхронизация тоже не нужна, так как все добавление
        // происходит в одном потоке
        Platform.runLater(() -> linesList.add(string));
    }

    /**
     * Удаление потока из списка
     * @param linesReader поток для удаления из списка
     */
    private void removeLinesReader(LinesReader linesReader) {
        Platform.runLater(() -> linesReaders.remove(linesReader));
    }

    /**
     * Вызывается после завершения приложения.
     * Необходимо остановить все работающие потоки
     */
    public void stop() {
        linesReaders.stream().forEach(LinesReader::interrupt);
    }

    /**
     * Вызывается при нажатии на кнопку остановки потока
     */
    @FXML
    private void stopThreadAction(ActionEvent actionEvent) {
        // Остановка всех выбранных в списке потоков
        linesReadersListView.getSelectionModel().getSelectedItems().stream()
                .forEach(LinesReader::interrupt);
    }

    /**
     * Класс, необходимый для работы TextField с числами.
     * При каждом изменении текста происходит проверка текста на
     * возможность преобразования к числу.
     * При изменении числа происходит вызов соответствующей функции, полученной при создании объекта.
     */
    class IntegerStringChangeListener implements ChangeListener<String> {

        private final IntConsumer consumer;
        private final TextField textField;
        private int value;

        IntegerStringChangeListener(TextField textField, IntConsumer consumer) {
            this.textField = textField;
            this.consumer = consumer;
        }

        @Override
        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
            if ("".equals(newValue))
                return;
            try {
                value = Integer.parseInt(newValue);
                consumer.accept(value);
            } catch (NumberFormatException ignored) {
                textField.setText(oldValue);
            }
        }
    }

    /**
     * Вызывается при смене языка в ComboBox
     */
    @FXML
    private void changeLang(ActionEvent event) {
        String lang = langComboBox.getValue();
        setLang(lang);
    }

    /**
     * Смена языка приложения. Происходит смена ResourceBundle в контроллере
     * в соответствии с переданным языком, а так же установка нового текста
     * во всех необходимых элементах
     * @param lang новый язык приложения
     */
    private void setLang(String lang) {
        Locale locale = new Locale(lang);
        resources = ResourceBundle.getBundle("application.lang", locale);

        startThreadButton.setText(resources.getString("startThreadText"));
        stopThreadButton.setText(resources.getString("stopThreadsText"));
        saveSettingsButton.setText(resources.getString("saveSettingsText"));
        loadSettingsButton.setText(resources.getString("loadSettingsText"));

        linesAreaTab.setText(resources.getString("linesAreaText"));
        controlPanelTab.setText(resources.getString("controlPanelText"));
        settingsTab.setText(resources.getString("settingsPanelText"));

        availableCountLinesLabel.setText(resources.getString("availableCountLinesText"));
        readSpeedLabel.setText(resources.getString("readSpeedText"));
        oddLinesColorLabel.setText(resources.getString("oddLinesColorText"));
        evenLinesColorLabel.setText(resources.getString("evenLinesColorText"));
        langChooserLabel.setText(resources.getString("langChooserText"));
    }

    /**
     * Вызывается при нажатии на кнопку сохранения настроек
     */
    @FXML
    private void saveSettingsAction(ActionEvent event) {
        if (connection == null)
            return;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(checkSettingsTableExist);
            if (!resultSet.next()) {
                System.err.println("+");
                PreparedStatement createSettingsTableStatement = connection.prepareStatement(createSettingsTable);
                createSettingsTableStatement.executeUpdate();
            }
            resultSet.close();
            statement.executeUpdate(removeSettings);
            String oddLinesColor = toHexString(oddLinesColorPicker.getValue());
            String evenLinesColor = toHexString(evenLinesColorPicker.getValue());
            PreparedStatement saveSettingsStatement = connection.prepareStatement(saveSettings);
            saveSettingsStatement.setInt(1, availableCountLines);
            saveSettingsStatement.setLong(2, readSpeedMillis);
            saveSettingsStatement.setString(3, oddLinesColor);
            saveSettingsStatement.setString(4, evenLinesColor);
            saveSettingsStatement.setString(5, langComboBox.getValue());
            saveSettingsStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Вызывается при нажатии на кнопку загрузки настроек
     */
    @FXML
    private void loadSettingsAction(ActionEvent event) {
        if (connection == null)
            return;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(selectSettings);
            if (!resultSet.next())
                return;
            availableCountLinesTextField.setText(String.valueOf(resultSet.getInt("availableCountLines")));
            readSpeedTextField.setText(String.valueOf(resultSet.getInt("readSpeedMillis")));
            String oddLinesColor = resultSet.getString("oddCellsColor");
            oddLinesColorPicker.setValue(Color.web(oddLinesColor));
            String evenLinesColor = resultSet.getString("evenCellsColor");
            evenLinesColorPicker.setValue(Color.web(evenLinesColor));
            langComboBox.setValue(resultSet.getString("lang"));
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
