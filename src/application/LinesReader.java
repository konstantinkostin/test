package application;

import java.io.*;
import java.util.function.Consumer;

/**
 * Класс для считывания с определенным интервалом строк из файла.
 * После считывания очередной строки, а так же после хаветшения считывания,
 * будет вызвана переданная в конструктор функция.
 *
 */
public class LinesReader extends Thread {

    private final BufferedReader bufferedReader;
    private final String filename;

    private final Consumer<String> lineReadedConsumer;
    private final Consumer<LinesReader> finalConsumer;

    // Период для считывания строки
    // Значение может быть изменено из другого потока, поэтому необходимо ключевое слово volatile.
    // Это охначает, что значение переменной не будет помещено в кеш после считывания и
    // и мы всегда имеем актуальное значение.
    private volatile long readSpeedMillis;

    public LinesReader(File file, long readSpeedMillis, Consumer<String> lineReadedConsumer, Consumer<LinesReader> finalConsumer) throws FileNotFoundException {
        this.lineReadedConsumer = lineReadedConsumer;
        this.finalConsumer = finalConsumer;
        this.bufferedReader = new BufferedReader(new FileReader(file));
        this.filename = file.getName();
        this.readSpeedMillis = readSpeedMillis;
    }

    @Override
    public void run() {
        try {
            // Флаг isInterrupted (который получается через метод interrupted)
            // устанавливается в случае необходимости завершения потока,
            // поэтому считываем строки либо до конца файла, либо до принудительного завершения
            while (!Thread.interrupted()) {
                String s = bufferedReader.readLine();
                if (s == null)
                    break;
                lineReadedConsumer.accept(s);
                Thread.sleep(readSpeedMillis);
            }
        } catch (IOException e) {

        } catch (InterruptedException e) {

        }
        finally {
            // Что бы не произошло в процессе работы, необходимо сообщить о завершении своей работы
            finalConsumer.accept(this);
        }
    }

    public void setReadSpeed(long readSpeedMillis) {
        this.readSpeedMillis = readSpeedMillis;
    }

    public String getFilename() {
        return filename;
    }

    @Override
    public String toString() {
        return filename;
    }
}
