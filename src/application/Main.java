package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TreeSet;

public class Main extends Application {

    Controller controller = null;

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("application.fxml"));
        loader.setResources(ResourceBundle.getBundle("application.lang", new Locale("en")));
        Parent root = loader.load();
        controller = loader.getController();
//        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
//        File.cre
//        String url = "jdbc:postgresql://192.168.56.101:5432/settings";
//        //Имя пользователя БД
//        String name = "the";
//        //Пароль
//        String password = "password";
//        //Загружаем драйвер
//        Class.forName("org.postgresql.Driver");
//        System.out.println("Драйвер подключен");
//        //Создаём соединение
//        Connection connection = DriverManager.getConnection(url, name, password);
//        System.out.println("Соединение установлено");
//        connection.close();

//        TreeSet<Integer> a;
//        a.add()
    }

    @Override
    public void stop() {
        if (controller == null)
            return;
        controller.stop();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
